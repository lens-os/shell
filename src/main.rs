// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#![no_std]
#![no_main]

extern crate lens_flows;
extern crate lens_files;
extern crate lens_process;

use lens_flows::byte::{Read, Write};

#[no_mangle]
fn main() {
    let stdout =
        lens_files::File::from_descriptor(1).expect("Failed to open stdout");
    let stdin = lens_files::File::from_descriptor(0).expect("Failed to open stdin");
    stdout
        .write(b"LENS shell\n")
        .expect("Failed to write stdout");
    loop {
        stdout.write(b"lens> ").expect("Failed to write stdout");
        let mut read_vec = stdin.read(128).expect("Failed to read stdin");
        let len = read_vec.len();
        let read = core::str::from_utf8(&mut read_vec[..len - 1])
            .expect("Failed to validate input as UTF-8");
        let name = &read[..];
        if name == "exit" {
            break;
        }
        let template = lens_process::launch::ProcessTemplate::new_basic(read);
        template.run().expect("Failed to run process");
    }
}
